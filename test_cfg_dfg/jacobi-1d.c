 #include <string.h>
#include <stdio.h>
#include <stdbool.h>

#define SIZE 100
#define _PB_N 100
#define _PB_TSTEPS 100
int A[SIZE],B[SIZE];
void jacobi_1d(){
	int t,i;
	for (t = 0; t < _PB_TSTEPS; t++)
	{
		for (i = 1; i < _PB_N - 1; i++)
			B[i] = 0.33333 * (A[i-1] + A[i] + A[i + 1]);
		for (i = 1; i < _PB_N - 1; i++)
			A[i] = 0.33333 * (B[i-1] + B[i] + B[i + 1]);
	}
}

int  main(){

	jacobi_1d();


}


