# cgra_compiler

LLVM pass build -

1. go to llvm-invokecgra-pass>build folder.
2. remove files in llvm-invokecgra-pass>build directory.
3. enter following commands inside llvm-invokecgra-pass>build directory;
  cmake ..
  make
4. libInvokeCGRA.so file must be created in llvm-invokecgra-pass>build>InvokeCGRA

Do the same for other passes as well

Run script -

5. Go to scripts folder
6. Open run_pass.sh
7. Change the toolchain path.
8. open new_opt.sh
9. Change the toolchain path. 
10. run script with following two arguments

./run_pass.sh Target_Function_Name na



It will print mappable units as following

Printing mappable unit map ... 

INNERMOST_LN11 :: for.body4,|entry=for.cond1.preheadertofor.body4,|
exit=for.body4tofor.cond.cleanup3,

POST_LN1 :: for.cond.cleanup,|entry=for.cond.cleanup3tofor.cond.cleanup,|exit=

POST_LN11 :: for.cond.cleanup3,|entry=for.body4tofor.cond.cleanup3,|

exit=for.cond.cleanup3tofor.cond.cleanup,for.cond.cleanup3tofor.cond1.preheader,

PRE_LN1 :: entry,|entry=|exit=entrytofor.cond1.preheader,

PRE_LN11 :: for.cond1.preheader,|

entry=for.cond.cleanup3tofor.cond1.preheader,entrytofor.cond1.preheader,|

exit=for.cond1.preheadertofor.body4,
 
11. Identify the mapping unit(Loop) name - Ex. INNERMOST_LN11
12. run ./run_pass.sh (Target Function Name) (Mapping Unit name)

Ex. 

./runpassh.sh test INNERMOST_LN11
 

