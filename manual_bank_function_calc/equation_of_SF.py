from array import *

#A = [[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]
#,[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]
w, h = 20, 20
A = [[0 for x in range(w)] for y in range(h)] 
E = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

def checkUnique(lst):
   return len(lst) == len(set(lst))

alphai = 0
alphaj = 0
N = 0
B = 0

non_conflict = 0
listT1 = []
listT2 = [] #access pattern 2
listT3 = [] #access pattern 2

for B in range(1,17):
    for N in range(4,16):
        non_conflict = 0
        for i in range(0,1):
            for j in range(0,64):
                #A[i][j]= ((20*i+j)//B)%N
                #print(j)
                E[j]= ((j)//B)%N
        count = 0
        for i in range(0,1):
            for j in range(0,58):
                #gemm array B
                #listT1.append(A[i][4*j]) 
                #listT1.append(A[i][4*j+1])
                #listT1.append(A[i][4*j+2])
                #listT1.append(A[i][4*j+3])

                #gemm array C
                #listT1.append(A[4*j][i]) 
                #listT1.append(A[4*j+1][i])
                #listT1.append(A[4*j+2][i])
                #listT1.append(A[4*j+3][i])

                #stencil 2D
                #listT1.append(A[i][j]) 
                #listT1.append(A[i-1][j])
                #listT1.append(A[i+1][j])
                #listT1.append(A[i][j-1])
                #listT1.append(A[i][j+1])

                #hydro
                #listT1.append(E[j+1]) 
                #listT1.append(E[j+3])

                #2dehf
                #zp,zq
                #listT1.append(A[i+1][j-1]) 
                #listT1.append(A[i][j-1])
                #listT1.append(A[i][j])

                #zr
                #listT1.append(A[i-1][j]) 
                #listT1.append(A[i][j-1])
                #listT1.append(A[i][j])

                #zm
                #listT1.append(A[i+1][j-1]) 
                #listT1.append(A[i][j-1])
                #istT1.append(A[i][j])

                #eq of sf
                listT1.append(E[j])
                listT1.append(E[j+1])
                listT1.append(E[j+2])
                listT1.append(E[j+3])#2
                listT1.append(E[j+4])#2
                listT1.append(E[j+5])#2
                listT1.append(E[j+6])#2

                
 

               # u[k] + r*( z[k] + r*y[k] ) +
                #   t*( u[k+3] + r*( u[k+2] + r*u[k+1] ) +
                 #     t*( u[k+6] + q*( u[k+5] + q*u[k+4] 


         #   ( zp[k+1][j-1] +zq[k+1][j-1] -zp[k][j-1] -zq[k][j-1] )*
          #            ( zr[k][j] +zr[k][j-1] ) / ( zm[k][j-1] +zm[k+1][j-1]);
          # zb[k][j] = ( zp[k][j-1] +zq[k][j-1] -zp[k][j] -zq[k][j] ) *
           #           ( zr[k][j] +zr[k-1][j] ) / ( zm[k][j] +zm[k][j-1]);


                if checkUnique(listT1):
                    non_conflict = non_conflict+1
                #if checkUnique(listT2):
                #   non_conflict = non_conflict+1
                #count = count+2
                count = count+1


                listT1 = []
                listT2 = []
                listT3 = []
                listT4 = []
               # if ((alphai==(-9)) & (alphaj==-5) & (N==4) & (B==2)):
                 #   print(A[i][j-1] , A[i-1][j] , A[i+1][j] , A[i][j+1],non_conflict)


        #if ((alphai==(-5)) & (alphaj==-1) & (N==4) & (B==2)):
        #    print(A[0][1] , A[1][0] , A[1][2] , A[2][1],non_conflict))

        if non_conflict==(count) :
                                #pass
                                print(non_conflict," N = ",N," B = ",B)

                           
                        
                #print("\n")