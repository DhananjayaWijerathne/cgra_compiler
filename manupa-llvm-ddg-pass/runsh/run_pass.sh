rm *_looptrace.log
rm *_loopinfo.log
rm *_munittrace.log
rm memtraces/*
toolchain="/home/dmd/workplace/CGRA/cgra_compiler"

#clang --target=armv7a-linux-eabi -m32 -c -emit-llvm -Os -fno-tree-vectorize ../fft.c -S -o integer_fft.ll
clang -target i386-unknown-linux-gnu -malign-double -c -emit-llvm -Os -fno-tree-vectorize radix_4_zhig.c -S -o integer_fft.ll
opt -gvn -mem2reg -memdep -memcpyopt -lcssa -loop-simplify -licm -loop-deletion -indvars -simplifycfg -mergereturn -dot-cfg -gvn integer_fft.ll -o integer_fft_gvn.ll
#opt -load ~/manycore/cgra_dfg/buildeclipse/skeleton/libSkeletonPass.so -fn $1 -ln $2 -ii $3 -skeleton integer_fft_gvn.ll -S -o integer_fft_gvn_instrument.ll

opt -load $toolchain/manupa-llvm-ddg-pass/build/skeleton/libSkeletonPass.so -fn $1 -munit $2 -skeleton integer_fft_gvn.ll -S -o integer_fft_gvn_instrument.ll

#clang -target i386-unknown-linux-gnu -c -emit-llvm -S ~/workplace/CGRA/hycube-compiler/skeleton/instrumentation/instrumentation.cpp -o instrumentation.ll
#llvm-link integer_fft_gvn_instrument.ll instrumentation.ll -o final.ll

#llc -filetype=obj final.ll -o final.o
#g++ -m32 final.o -o final
