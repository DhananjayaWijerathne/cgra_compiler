; ModuleID = 'radix_4_zhig.c'
source_filename = "radix_4_zhig.c"
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i386-unknown-linux-gnu"

@N = dso_local local_unnamed_addr global i32 64, align 4
@STAGE_RADIX = dso_local local_unnamed_addr global [3 x i32] [i32 4, i32 4, i32 4], align 4
@mem = common dso_local local_unnamed_addr global [64 x i32] zeroinitializer, align 4

; Function Attrs: norecurse nounwind optsize
define dso_local void @test() local_unnamed_addr #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @STAGE_RADIX, i32 0, i32 1), align 4, !tbaa !3
  %1 = load i32, i32* getelementptr inbounds ([3 x i32], [3 x i32]* @STAGE_RADIX, i32 0, i32 2), align 4, !tbaa !3
  %mul = mul nsw i32 %1, %0
  %mul14 = shl i32 %mul, 1
  %mul19 = mul nsw i32 %mul, 3
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %for.cond.cleanup3, %entry
  %s2_idx.0114 = phi i32 [ 0, %entry ], [ %inc52, %for.cond.cleanup3 ]
  br label %for.body4

for.cond.cleanup:                                 ; preds = %for.cond.cleanup3
  ret void

for.cond.cleanup3:                                ; preds = %for.body4
  %inc52 = add nuw nsw i32 %s2_idx.0114, 1
  %exitcond115 = icmp eq i32 %inc52, 4
  br i1 %exitcond115, label %for.cond.cleanup, label %for.cond1.preheader

for.body4:                                        ; preds = %for.body4, %for.cond1.preheader
  %s1_idx.0113 = phi i32 [ 0, %for.cond1.preheader ], [ %inc, %for.body4 ]
  %mul6 = mul nsw i32 %s1_idx.0113, %1
  %add7 = add nsw i32 %mul6, %s2_idx.0114
  %arrayidx8 = getelementptr inbounds [64 x i32], [64 x i32]* @mem, i32 0, i32 %add7
  %2 = load i32, i32* %arrayidx8, align 4, !tbaa !3
  %add12 = add i32 %add7, %mul
  %arrayidx13 = getelementptr inbounds [64 x i32], [64 x i32]* @mem, i32 0, i32 %add12
  %3 = load i32, i32* %arrayidx13, align 4, !tbaa !3
  %add17 = add i32 %add7, %mul14
  %arrayidx18 = getelementptr inbounds [64 x i32], [64 x i32]* @mem, i32 0, i32 %add17
  %4 = load i32, i32* %arrayidx18, align 4, !tbaa !3
  %add22 = add i32 %add7, %mul19
  %arrayidx23 = getelementptr inbounds [64 x i32], [64 x i32]* @mem, i32 0, i32 %add22
  %5 = load i32, i32* %arrayidx23, align 4, !tbaa !3
  %sub = sub nsw i32 %2, %4
  %sub26 = sub nsw i32 %3, %5
  %add25 = add i32 %3, %2
  %add24 = add i32 %add25, %4
  %add27 = add i32 %add24, %5
  %add29 = add nsw i32 %sub26, %sub
  store i32 %add27, i32* %arrayidx8, align 4, !tbaa !3
  store i32 %add29, i32* %arrayidx13, align 4, !tbaa !3
  store i32 %add27, i32* %arrayidx18, align 4, !tbaa !3
  store i32 %add29, i32* %arrayidx23, align 4, !tbaa !3
  %inc = add nuw nsw i32 %s1_idx.0113, 1
  %exitcond = icmp eq i32 %inc, 4
  br i1 %exitcond, label %for.cond.cleanup3, label %for.body4
}

; Function Attrs: norecurse nounwind optsize
define dso_local void @main() local_unnamed_addr #0 {
entry:
  %0 = load i32, i32* @N, align 4, !tbaa !3
  %cmp5 = icmp sgt i32 %0, 0
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.body, %entry
  tail call void @test() #1
  ret void

for.body:                                         ; preds = %entry, %for.body
  %i.06 = phi i32 [ %inc, %for.body ], [ 0, %entry ]
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* @mem, i32 0, i32 %i.06
  store i32 %i.06, i32* %arrayidx, align 4, !tbaa !3
  %inc = add nuw nsw i32 %i.06, 1
  %cmp = icmp slt i32 %inc, %0
  br i1 %cmp, label %for.body, label %for.cond.cleanup
}

attributes #0 = { norecurse nounwind optsize "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="pentium4" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { optsize }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"NumRegisterParameters", i32 0}
!1 = !{i32 1, !"wchar_size", i32 4}
!2 = !{!"clang version 8.0.0 (https://git.llvm.org/git/clang.git/ e24ad3390ffe53acb284a1a6288bad22134bf80a) (https://git.llvm.org/git/llvm.git/ ee609f0f123c4b21a78094ff4ec3e4fe8e3be7ce)"}
!3 = !{!4, !4, i64 0}
!4 = !{!"int", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C/C++ TBAA"}
