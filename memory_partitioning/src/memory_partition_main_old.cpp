
//============================================================================
// Name        : mem_partitioner.cpp
// Author      : Dhananjaya Wijerathne
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <assert.h>
#include <stdlib.h>
#include "mem_partitioner.h"

using namespace std;

#define ALPHA_FROM 1
#define ALPHA_TO 2
#define B_FROM 1 //2th power numbers
#define B_TO 17
#define N_FROM 4
#define N_TO 5

#define NUMBER_OF_INTEGER_POINTS_IN_ITERATION_DOMAIN 16

/* read access pattern and context */
void read_access_patterns(int& num_of_stages,int& num_of_accesses,int& data_dimensions, int& iteration_dimensions,
		vector<vector<vector<vector<int> > > >&multstage_matrixA, vector<vector<vector<int> > >&multstage_matrixC , vector<vector<int> >&contextMatrix  , vector<int>&iterationBoundVec){

	ifstream f("64_point_fft.txt");//("first_diff.txt");//("gemm.txt");//("64_point_fft.txt");
	f >> num_of_stages;
	f >> num_of_accesses;
	f >> data_dimensions >> iteration_dimensions;

	for(int s=0; s< num_of_stages;s++){
		vector<vector<vector<int> > > matrixA_3D;
		vector<vector<int> > matrixC_2D;
		for(int k=0; k < num_of_accesses;k++){
			//Read matrix A
			std::vector<std::vector<int> > matrix;

			for (int i = 0; i < data_dimensions; i++) {
				vector<int> row; // Create an empty row
				int temp;
				for (int j = 0; j < iteration_dimensions; j++) {
					f >> temp;
					row.push_back(temp); // Add an element (column) to the row
				}
				matrix.push_back(row); // Add the row to the main vector
			}
			matrixA_3D.push_back(matrix);
			//Read matrix C

			vector<int> column; // Create an empty column matrix
			int temp;
			for (int i = 0; i < data_dimensions; i++) {
				f >> temp;
				column.push_back(temp); // Add an element
			}
			matrixC_2D.push_back(column); // Add the column to the main vector

		}
		multstage_matrixA.push_back(matrixA_3D);
		multstage_matrixC.push_back(matrixC_2D);
	}
	//Reading Context matrix
	int m,n;
	f >> m >> n;
	for (int i = 0; i < m; i++) {
    	vector<int> row; // Create an empty row
    	int temp;
    	for (int j = 0; j < n; j++) {
    		f >> temp;
        	row.push_back(temp); // Add an element (column) to the row
    	}
    	contextMatrix.push_back(row); // Add the row to the main vector
	}

	for (int i = 0; i < m; i++){
		int temp;
		f >> temp;
		iterationBoundVec.push_back(temp);
	}


}

void print_access_patterns(int num_of_accPtrns,int data_dimensions, int iteration_dimensions,
		vector<vector<vector<int> > > matrixA_vec, vector<vector<int> > matrixC_vec){
	for(int k=0; k < num_of_accPtrns;k++){
		for (int i = 0; i < data_dimensions; i++) {
	    	for (int j = 0; j < iteration_dimensions; j++) {
	    		cout << matrixA_vec[k][i][j] << " ";
	    	}
	    	cout << endl ;
		}
		cout << endl ;

    	for (int i = 0; i < data_dimensions; i++) {
    		cout << matrixC_vec[k][i] << " ";
    		cout << endl ;
    	}
    	cout << endl ;
	}
}


//Create conflict polytope for two access patterns
void create_conflict_polytope(int data_dimensions,int iteration_dimensions,vector<int> alpha,int B,int N,vector<vector<int> > A_0,
		vector<vector<int> > A_1, vector<int> C_0, vector<int> C_1, vector<vector<int> >& conflict_polytope ){


	vector<int> row1,row2,row3,row4; // Create an empty row
	//first four rows of conflict polytope
	// 1 <-alpha*A0> B*N B <0> B-1-alpha*C0
	// 1 <alpha*A0> -B*N -B <0> alpha*C0
	// 1 <-alpha*A1> 0 B <0> B-1-alpha*C1
	// 1 <alpha*A1> 0 -B <0> alpha*C1
	row1.push_back(1);row2.push_back(1);row3.push_back(1);row4.push_back(1);
	//--
	int sum0=0;
	int sum1=0;
	for(int j=0;j<iteration_dimensions;j++){
		for(int i=0;i<data_dimensions;i++){
		   sum0 = sum0 + (alpha[i])*A_0[i][j];//matrixA_vec[0][i][j];
		   sum1 = sum1 + (alpha[i])*A_1[i][j];//matrixA_vec[1][i][j];
		}
		row1.push_back(-sum0);row2.push_back(sum0);
		row3.push_back(-sum1);row4.push_back(sum1);
		sum0 = 0;sum1=0;
	}
	//--
	row1.push_back(B*N);row2.push_back(-(B*N));row3.push_back(0);row4.push_back(0);
	row1.push_back(B);row2.push_back(-B);row3.push_back(B);row4.push_back(-B);
	//--
	for(int i=0;i<iteration_dimensions;i++){
		row1.push_back(0);row2.push_back(0);row3.push_back(0);row4.push_back(0);
	}
	//--

	for(int i=0;i<data_dimensions;i++){
		sum0 = sum0 + (alpha[i])*C_0[i];//matrixC_vec[0][i];
		sum1 = sum1 + (alpha[i])*C_1[i];//matrixC_vec[1][i];
	}
	row1.push_back(B-1-sum0);row2.push_back(sum0);row3.push_back(B-1-sum1);row4.push_back(sum1);
	conflict_polytope.push_back(row1);
	conflict_polytope.push_back(row2);
	conflict_polytope.push_back(row3);
	conflict_polytope.push_back(row4);
	int polytope_width = row1.size();
	//cout << "Polytope Width = "<< polytope_width << endl;
	row1.clear();row2.clear();row3.clear();row4.clear();

	for(int i=0;i<iteration_dimensions;i++){
		row1.push_back(1);//FIXME
		for(int k=0;k<polytope_width-1;k++){
			if(i==k){
				row1.push_back(1);
			}else{
				row1.push_back(0);
			}
		}
		conflict_polytope.push_back(row1);
		row1.clear();
	}

	for(int i=0;i<iteration_dimensions;i++){
		row1.push_back(1);//FIXME
		for(int k=0;k<polytope_width-1;k++){
			if(i==k){
				row1.push_back(-1);
			}else if (k==(i+iteration_dimensions+2)) {
				row1.push_back(1);
			}
			else{
				row1.push_back(0);
			}
		}
		conflict_polytope.push_back(row1);
		row1.clear();
	}



}


void print_conflict_polytope(int data_dimensions,int iteration_dimensions, vector<vector<int> > conflict_polytope){
	for(int m=0;m<(4+2*iteration_dimensions);m++){
		for(int n=0;n<(1+2*iteration_dimensions+2+1);n++){
    		cout<< conflict_polytope[m][n] << "\t";
    	}
		cout << endl;
    }
	cout << endl;
}

void write_conflict_polytope_and_context(int data_dimensions,int iteration_dimensions, vector<vector<int> > conflict_polytope, vector<vector<int> > contextMatrix){
	ofstream myfile;
	 myfile.open ("transmit_conflict_polytope.in");
	// myfile << conflict_polytope.size() << "\t" << conflict_polytope[0].size() << "\n";

	 assert((4+2*iteration_dimensions)== conflict_polytope.size());
	 assert((1+2*iteration_dimensions+2+1)== conflict_polytope[0].size());

	for(int m=0;m<conflict_polytope.size(); m++){//(4+2*iteration_dimensions);m++){
		for(int n=0;n<conflict_polytope[0].size();n++){//(1+2*iteration_dimensions+2+1);n++){
    		myfile<< conflict_polytope[m][n] << "\t";
    	}
		myfile << "\n";
    }
	myfile << "\n";

	ofstream myfile2;
	myfile2.open ("transmit_context_matrix.in");

	//myfile2 << contextMatrix.size() << "\t" << contextMatrix[0].size() << "\n";

	for(int m=0;m<(contextMatrix.size());m++){
		for(int n=0;n<(contextMatrix[0].size());n++){
    		myfile2<< contextMatrix[m][n] << "\t";
    	}
		myfile2 << "\n";
    }

	myfile2 << "\n";

	myfile.close();
	myfile2.close();

}


void comb(int N, int K, vector<vector<int> >& combinations)
{
    std::string bitmask(K, 1); // K leading 1's
    bitmask.resize(N, 0); // N-K trailing 0's
    // print integers and permute bitmask
    do {
    	std::vector<int> row;
        for (int i = 0; i < N; ++i) // [0..N-1] integers
        {
            if (bitmask[i]) {row.push_back(i);}//std::cout << " " << i;}
        }
        combinations.push_back(row);
        std::cout << std::endl;
    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
}

int main() {
	cout << "!!!Memory Partitioner!!!" << endl; // prints !!!Hello World!!!
	//(4, vector<int>(4));;
	std::vector<std::vector<std::vector<std::vector<int> > > > multstage_matrixA;
	std::vector<std::vector<std::vector<int> > > multstage_matrixC;
	std::vector<int> iterationBoundVec;
	std::vector<std::vector<int> > contextMatrix;
	int num_of_stages;//in one stage
	int num_of_accesses;//in one stage
	int data_dimensions,iteration_dimensions;
	int num_of_int_points;

	//Read access pattern and context matrix
	read_access_patterns(num_of_stages,num_of_accesses,data_dimensions, iteration_dimensions,multstage_matrixA, multstage_matrixC , contextMatrix,iterationBoundVec);

	print_access_patterns(num_of_accesses,data_dimensions, iteration_dimensions,multstage_matrixA[2], multstage_matrixC[2] );

	std::vector<int> alpha;
	int B=4,N=4;
	std::vector<std::vector<int> > conflict_polytope;


	vector<vector<int> > combinations;

	int *ptr;
	ptr = (int*) malloc(iteration_dimensions*sizeof(int));

	for(int i = 0; i<iteration_dimensions;i++){
		ptr[i] = iterationBoundVec[i];//CHECK IF ORDER NEED TO BE CHANGED
	}

	comb(num_of_accesses,2,combinations);

	for (int i1=ALPHA_FROM;i1<ALPHA_TO;i1++){
		for(int i2=ALPHA_FROM;i2<ALPHA_TO;i2++){//FIXME
			alpha.clear();
			alpha.push_back(i1);alpha.push_back(i2);
			for(B=B_FROM;B<B_TO;B=2*B){
				for(N=N_FROM;N<N_TO;N++){
					for(int s=0;s<num_of_stages;s++){
						for(int c=0;c<combinations.size();c++){

							create_conflict_polytope(data_dimensions,iteration_dimensions,alpha,B,N,
									multstage_matrixA[s][combinations[c][0]],multstage_matrixA[s][combinations[c][1]],
									multstage_matrixC[s][combinations[c][0]],multstage_matrixC[s][combinations[c][1]],
									conflict_polytope );
							//cout << "-----Conflict polytope for access pattern "<< combinations[c][0] <<" and "<<combinations[c][1] << "-------"<< endl;
							//print_conflict_polytope(data_dimensions,iteration_dimensions,conflict_polytope);

						 // TODO: Add polylib API calls here

							write_conflict_polytope_and_context(data_dimensions,iteration_dimensions,conflict_polytope,contextMatrix);

							//cout << "-----Poly called" << endl;
							num_of_int_points = poly(conflict_polytope.size(),conflict_polytope[0].size(),contextMatrix.size(),contextMatrix[0].size(),ptr);
							//cout << "-----Poly Num of int points: "<< num_of_int_points << endl;
							conflict_polytope.clear();
							if(num_of_int_points > 0) break;
							if(c==(combinations.size()-1)){
								cout << "Stage "<< s <<" Solution:-------Alpha=("<< alpha[0] <<"," <<alpha[1] <<"), B="<<B<<", N="<<N <<" ---------"<< endl;


								for(int s_ =0; s_ < num_of_stages ;s_++){
									if(s_!=s){
										for(int c_=0;c_<combinations.size();c_++){
											create_conflict_polytope(data_dimensions,iteration_dimensions,alpha,B,N,
																				multstage_matrixA[s_][combinations[c][0]],multstage_matrixA[s_][combinations[c][1]],
																				multstage_matrixC[s_][combinations[c][0]],multstage_matrixC[s_][combinations[c][1]],
																				conflict_polytope );
											write_conflict_polytope_and_context(data_dimensions,iteration_dimensions,conflict_polytope,contextMatrix);
											num_of_int_points = poly(conflict_polytope.size(),conflict_polytope[0].size(),contextMatrix.size(),contextMatrix[0].size(),ptr);

											//cout << "-----Poly Num of int points: "<< num_of_int_points << endl;
											conflict_polytope.clear();
											if(num_of_int_points != NUMBER_OF_INTEGER_POINTS_IN_ITERATION_DOMAIN){break;}
											if(c_ == (combinations.size()-1)){
												cout << "The solution of stage "<< s << " can be added to solution of stage "<<s_<<" without violating the conflict free condition" << endl;
												//cout << "-----Poly Num of int points: "<< num_of_int_points << endl;
											}
										}


									}
								}

							}
							//cout << i1 << i2<< B << N << c << endl;
						}


					}
				}
			}
		}
	}


	/*
		 * TODO
		 *
		 * if(count_points(conflict_polytope) == 0) for all combinations
		 * alpha, B and N is solution
		 *
		 * for that alpha,B,N solution check,
		 * count_points(conflict_polytopes combinations of next stages )== number of points in data domain
		 * this solution can be added with the next stage's solution without harming the next stages solutions
		 *
		 * */


	return 0;
}
