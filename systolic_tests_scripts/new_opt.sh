#!/bin/bash
#Author: Shail Dave
file="$1"
name=${file%.ll}

llfile="$name.ll"

toolchain="/home/dmd/workplace/CGRA/cgra_compiler"
llvmbin="$toolchain/llvm/build/bin"
llvmlib="$toolchain/llvm/build/lib"
llvmbuild="$toolchain/llvm/build"

#opt -strip-debug -O3 $llfile -o temp.bc
#llvm-dis temp.bc -o $llfile

opt -load $toolchain/ccf-llvm-ddg-pass/build/DDGGen/libDDGGen.so -DDGGen $llfile -o temp.bc 
llvm-dis temp.bc -o temporary.ll
cp temp.bc $name.bc

# Check whether the directory CGRAExec exists
#if [ -d "CGRAExec" ]; then
        opt -load $toolchain/ccf-llvm-invokecgra-pass/build/InvokeCGRA/libInvokeCGRA.so -InvokeCGRA temp.bc -o temp1.bc
        llvm-dis temp1.bc -o temporaryIR.ll
        #$llvmbuild/bin/opt -load $llvmbuild/lib/LLVMCGRAGen.so -CGRAGen temp1.bc -o $name.bc
#fi

exit
