#include <string.h>
#include <stdio.h>
   
 int spm_a[100];
 int spm_b[100];
 int spm_c[100];
 int spm_count;
 int spm_j;


void test()
{

	int a[100];
    int b[100];
    int c[100];
    volatile int count = 6;
    int i;

 /*   int spm_a[10];
    int spm_b[10];
    int spm_c[10];
    int spm_count;
    int spm_j;
*/
    //Initialize
   for(i=0;i<100;i++){
    a[i] = i;
    b[i] = i;
    c[i] = 0;
   }

   memcpy(spm_a,a,100*sizeof(int));
   memcpy(spm_b,b,100*sizeof(int));
   memcpy(&spm_count,&count,sizeof(int));

   #pragma CGRA
   for(spm_j=0;spm_j<100;spm_j++){
      spm_c[spm_j] = spm_a[spm_j] + spm_b[spm_j];
      spm_count = spm_count + 1;
   }

   memcpy(c, spm_c, 100*sizeof(int));
   memcpy(&count, &spm_count, sizeof(int));

   //Test
   for(i=0;i<100;i++){
   	printf("c[%d] = %d \n",i,c[i]);
   }

   printf("Count = %d\n",count);


}


int main(void){

	test();
	return 0;

}
