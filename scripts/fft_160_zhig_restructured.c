#include <string.h>
#include <stdio.h>

#define N 160

int mem[N] = {3,4,5,6,8,9};
int mem_test[N] = {3,4,5,6,8,9};

int STAGE_RADIX[3] = {8,5,4};


void fft(){



    // do 4 radix-4 of STAGE 0
    int s_idx = 0;
    int NS = N/STAGE_RADIX[s_idx];
    int i=0;
    int s1s2= STAGE_RADIX[1]*STAGE_RADIX[2];//the value is s1*s2
    int s2 = STAGE_RADIX[2];

    int LB1 = STAGE_RADIX[1];
    int LB2 = STAGE_RADIX[2];

    printf("Stage 1 \n");
    //8x5x4 n0 x n1 x n2
    for(int s2_idx = 0; s2_idx < LB2; s2_idx++){
       for(int s1_idx = 0; s1_idx < LB1; s1_idx++){
            int v0 = mem[s1s2*0 + s2*s1_idx + s2_idx];
            int v1 = mem[s1s2*1 + s2*s1_idx + s2_idx];
            int v2 = mem[s1s2*2 + s2*s1_idx + s2_idx];
            int v3 = mem[s1s2*3 + s2*s1_idx + s2_idx];
            int v4 = mem[s1s2*4 + s2*s1_idx + s2_idx];
            int v5 = mem[s1s2*5 + s2*s1_idx + s2_idx];
            int v6 = mem[s1s2*6 + s2*s1_idx + s2_idx];
            int v7 = mem[s1s2*7 + s2*s1_idx + s2_idx];

            int s0_0 = v0 + v2;
            int s0_1 = v1 + v3;
            int s0_2 = v0 - v2;
            int s0_3 = v1 - v3;
            // s0_3 = s0_3 * (-j) ignored

            int s1_0 = s0_0 + s0_1;
            int s1_1 = s0_0 - s0_1;
            int s1_2 = s0_2 + s0_3;
            int s1_3 = s0_2 - s0_3;

    
                   
            printf("v0:%d v1:%d v2:%d v3:%d v4:%d v5:%d v6:%d v7:%d \n", v0,v1,v2,v3,v4,v5,v6,v7);
       }
    }

    printf("Stage 2 \n");

    int s0s1 = STAGE_RADIX[0]*STAGE_RADIX[1];
    int LB0 = STAGE_RADIX[0];

    for(int s2_idx = 0; s2_idx < LB2; s2_idx++){
       for(int s0_idx = 0; s0_idx < LB0; s0_idx++){

            int s1_idx_base = s0_idx*LB1;

            int v0 = mem[LB2*(s1_idx_base+0) + s2_idx];
            int v1 = mem[LB2*(s1_idx_base+1) + s2_idx];
            int v2 = mem[LB2*(s1_idx_base+2) + s2_idx];
            int v3 = mem[LB2*(s1_idx_base+3) + s2_idx];
            int v4 = mem[LB2*(s1_idx_base+4) + s2_idx];

            int s0_0 = v0 + v2;
            int s0_1 = v1 + v3;
            int s0_2 = v0 - v2;
            int s0_3 = v1 - v3;
            // s0_3 = s0_3 * (-j) ignored

            int s1_0 = s0_0 + s0_1;
            int s1_1 = s0_0 - s0_1;
            int s1_2 = s0_2 + s0_3;
            int s1_3 = s0_2 - s0_3;

    
                   
            printf("v0:%d v1:%d v2:%d v3:%d v4:%d \n", v0,v1,v2,v3,v4);
       }
    }

    printf("Stage 3 \n");

    //8 x 5 x4 n0 x n1 x n2
    for(int s1_idx = 0; s1_idx < LB1; s1_idx++){
       for(int s0_idx = 0; s0_idx < LB0; s0_idx++){


            int s2_idx_base = (s0_idx + s1_idx*LB0)*LB2;

            int v0 = mem[s2_idx_base+0];
            int v1 = mem[s2_idx_base+1];
            int v2 = mem[s2_idx_base+2];
            int v3 = mem[s2_idx_base+3];

            int s0_0 = v0 + v2;
            int s0_1 = v1 + v3;
            int s0_2 = v0 - v2;
            int s0_3 = v1 - v3;
            // s0_3 = s0_3 * (-j) ignored

            int s1_0 = s0_0 + s0_1;
            int s1_1 = s0_0 - s0_1;
            int s1_2 = s0_2 + s0_3;
            int s1_3 = s0_2 - s0_3;

    
                   
            printf("v0:%d v1:%d v2:%d v3:%d \n", v0,v1,v2,v3);
       }
    }




}
void main(){
    for(int i = 0; i < N; i++){
       mem[i] = i;
    }

   /* int s_idx = 0;
    int NS = N/STAGE_RADIX[s_idx];
    int i = 0;
    
    int s1s2 = STAGE_RADIX[1]*STAGE_RADIX[2];
    int s2 = STAGE_RADIX[2];

    for(int s2_idx = 0; s2_idx < 4; s2_idx++){
    for(int s1_idx = 0; s1_idx < 4; s1_idx++){
        int v0 = mem[s1s2*0 + s2*s1_idx + s2_idx];
                int v1 = mem[s1s2*1 + s2*s1_idx + s2_idx];
                int v2 = mem[s1s2*2 + s2*s1_idx + s2_idx];
                int v3 = mem[s1s2*3 + s2*s1_idx + s2_idx];

                int s0_0 = v0 + v2;
                int s0_1 = v1 + v3;
                int s0_2 = v0 - v2;
                int s0_3 = v1 - v3;
                // s0_3 = s0_3 * (-j) ignored

                int s1_0 = s0_0 + s0_1;
        int s1_1 = s0_0 + s0_1;
        int s1_2 = s0_2 + s0_3;
        int s1_3 = s0_2 + s0_3;

        mem[s1s2*0 + s2*s1_idx + s2_idx] = s1_0;
        mem[s1s2*1 + s2*s1_idx + s2_idx] = s1_2;
        mem[s1s2*2 + s2*s1_idx + s2_idx] = s1_1;
        mem[s1s2*3 + s2*s1_idx + s2_idx] = s1_3;
               
                printf("v0:%d v1:%d v2:%d v3:%d \n", v0,v1,v2,v3);
    }
    }*/
fft();

}


