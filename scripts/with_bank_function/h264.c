   

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#define ioff 2
#define joff 2
bool  residue_transform_flag;
bool lossless_qpprime;
int max_imgpel_value,DQ_ROUND_8,DQ_BITS_8; 
int secondvalue,m_value;

int B1,B2,B3,B4,Bm,N;


inline int bank_offset(x){
   //return (((x>>B1 + x>>B2 + x>>B3 + x>>B4) & N)<<8) + ((x>>(N*Bm)*Bm + (x & Bm)));
  return (((x>>B1) & N)<<8) + ((x>>(N*Bm)*Bm + (x & Bm)));
  ///return (((x>>B1) & N)<<8);
}

void h264(){
  //from transfer 8X8
int m7[(8+ioff)*(8+ioff)],mpr[(8+ioff)*(8+joff)];
for( int i=0; i<8; i++)
  {
    for( int j=0; j<8; j++)
    {
      // Residue Color Transform
      if(!residue_transform_flag)
      {
          if(lossless_qpprime){
             secondvalue=m7[bank_offset((ioff + i) * (8+ioff)+joff + j)]+(long)mpr[bank_offset((i+ioff)*(8+ioff)+j+joff)];
             m_value=0>secondvalue?0:secondvalue; 
            m7[bank_offset((8+ioff)*(i+ioff)+j+joff)] =max_imgpel_value< m_value ? max_imgpel_value:m_value;
          }
      
        else{
          //m7[i+ioff][j+joff] =min(max_imgpel_value,max(0,(m7[ioff + i][joff + j]+((long)mpr[i+ioff][j+joff] << DQ_BITS_8)+DQ_ROUND_8)>>DQ_BITS_8));
           secondvalue=(m7[bank_offset((8+ioff)*(ioff + i)+joff + j)]+((long)mpr[bank_offset((8+ioff)*(i+ioff)+j+joff)] << DQ_BITS_8)+DQ_ROUND_8)>>DQ_BITS_8;
           m_value = 0>secondvalue?0:secondvalue;
          m7[bank_offset((8+ioff)*(i+ioff)+j+joff)] =max_imgpel_value< m_value ? max_imgpel_value:m_value;
        }
        
      }
      else
      {
        if(lossless_qpprime)
          m7[bank_offset((8+ioff)*(i+ioff)+j+joff)] = m7[bank_offset((8+ioff)*(ioff + i)+joff + j)];
        else
          m7[bank_offset((8+ioff)*(i+ioff)+j+joff)] =(m7[bank_offset((8+ioff)*(ioff + i)+joff + j)]+DQ_ROUND_8)>>DQ_BITS_8;
      }
    }
    }


}

void main(){

  h264();

 
}

