rm *_looptrace.log
rm *_loopinfo.log
rm *_munittrace.log
rm memtraces/*

toolchain="/home/dmd/workplace/CGRA/cgra_compiler"
#clang --target=armv7a-linux-eabi -m32 -c -emit-llvm -Os -fno-tree-vectorize ../fft.c -S -o integer_fft.ll
clang --target=armv7a-linux-eabi -isystem /usr/arm-linux-gnueabi/include -c -emit-llvm -Os  -fno-tree-vectorize -fno-builtin -disable-simplify-libcalls -fno-vectorize -fno-unroll-loops -fno-slp-vectorize  motivation.c -S -o code.ll
#stencil_2d.c radix_4_zhig gemm
#livermore = hydro_fragment, equation_of_SF, first_diff, 2d_ehf_1,with_bank_function/2d_ehf_1.c   with_bank_function/mpeg4.c correlation polybench/syr2k
clang --target=armv7a-linux-eabi -isystem /usr/arm-linux-gnueabi/include -c -emit-llvm -Os -fno-tree-vectorize cgra.c -S -o cgra.ll

llvm-link code.ll cgra.ll -S -o combinedIR.ll

#CCF passes
./new_opt.sh combinedIR.ll

#opt -gvn -mem2reg -memdep -memcpyopt -lcssa -loop-simplify -licm -loop-deletion -indvars -simplifycfg -mergereturn -dot-cfg -gvn integer_fft.ll -o integer_fft_gvn.ll
#opt -load ~/manycore/cgra_dfg/buildeclipse/skeleton/libSkeletonPass.so -fn $1 -ln $2 -ii $3 -skeleton integer_fft_gvn.ll -S -o integer_fft_gvn_instrument.ll

#opt -load ~/workplace/CGRA/hycube-compiler/build/skeleton/libSkeletonPass.so -fn $1 -munit $2 -ii $3 -dx $4 -dy $5 -skeleton integer_fft_gvn.ll -S -o integer_fft_gvn_instrument.ll

#clang -target i386-unknown-linux-gnu -c -emit-llvm -S ~/workplace/CGRA/hycube-compiler/skeleton/instrumentation/instrumentation.cpp -o instrumentation.ll
#llvm-link integer_fft_gvn_instrument.ll instrumentation.ll -o final.ll

#llc -filetype=obj final.ll -o final.o
#g++ -m32 final.o -o final

#DDG Gen Manupa
#opt -dot-cfg
opt -load $toolchain/manupa-llvm-ddg-pass/build/skeleton/libSkeletonPass.so -fn $1 -munit $2 -skeleton combinedIR.ll -S -o combinedIR_gvn_instrument.ll

opt -load $toolchain/manupa-llvm-ddg-pass_agi_removed/build/skeleton/libSkeletonPass.so -fn $1 -munit $2 -skeleton combinedIR.ll -S -o combinedIR_gvn_instrument.ll
